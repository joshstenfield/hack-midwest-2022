const express = require('express');
const { connect } = require('./db');
const app = express()
const port = process.env.PORT

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.listen(port, () => {
  connect().then(console.log);
  console.log(`Example app listening on port ${port}`)
});