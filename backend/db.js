var pg = require('pg');
const async = require('async');
const fs = require('fs');
const { callbackify } = require('util');
const { rows } = require('pg/lib/defaults');

const config = {
    host: 'us-central1.b6fc10e9-86b6-4322-b851-8eafdfbc878f.gcp.ybdb.io',
    port: '5433',
    database: 'yugabyte',
    user: 'hackmidwest',
    password: 'HackMidwest',
    // Uncomment and initialize the SSL settings for YugabyteDB Managed and other secured types of deployment
     ssl: {
         rejectUnauthorized: true,
         ca: fs.readFileSync('./yuga.crt').toString()
     },
    connectionTimeoutMillis: 5000
};

var client;

exports.connect = async () => {
    console.log('>>>> Connecting to YugabyteDB!');

    try {
        client = new pg.Client(config);

        await client.connect();

        console.log('>>>> Connected to YugabyteDB!');

    } catch (err) {
        console.error(err);
    }
}