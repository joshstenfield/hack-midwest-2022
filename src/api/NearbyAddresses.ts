export interface NearbyAddresses {
    addresses: Address[];
}

export interface Address {
    /**
     * The building name if available.
     */
    building: string
    
    /**
     * The house number or building number.
     */
    house_number: string
        
    /**
     * The road the nearby property is located in.
     */
    road: string
        
    /**
     * The city the nearby property is located in.
     */
    city: string
        
    /**
     * The state the nearby property is located in.
     */
    state: string
        
    /**
     * The county the nearby property is located in.
     */
    county: string    
    
    /**
     * The country_code of the nearby property.
     */
    country_code: string
        
    /**
     * The latitude of the nearby property.
     */
    lat: number
        
    /**
     * The longitude of the nearby property.
     */
    lng: number
        
    /**
     * A friendly value of the nearby property.
     */
    full_address: string
        
    /**
     * The zipcode of the nearby property.
     */
    zipcode: string    
}