import { useMemo, useState } from "react";
import Map from "../components/Map";
import { PropertyTabs } from "../components/PropertyTabs";

export default function Home() {
  const [selectedPlace, setSelectedPlace] = useState<any>(null);

  const address = useMemo(() => {
    if (selectedPlace) {
      if (selectedPlace.formatted_address) {
        const [streetAddress, city, stateZip, country] =
          selectedPlace.formatted_address.split(", ");
        if (stateZip) {
          const [state, zip] = stateZip.split(" ");
          return {
            streetAddress,
            city,
            state,
            zip,
            country,
          };
        }
      }
    }
    return null;
  }, [selectedPlace]);

  return (
    <div>
      <div
        style={{
          height: selectedPlace ? "40vh" : "100vh",
          width: "100%",
          marginTop: "64px",
        }}
      >
        <Map
          selectedPlace={selectedPlace}
          setSelectedPlace={(place: any) => {
            setSelectedPlace(place);
          }}
        />
      </div>
      {address && (
        <div style={{ height: "60vh" }}>
          <PropertyTabs property={address} />
        </div>
      )}
    </div>
  );
}
