export const getPropertyHash = (property: any) => {
  const params = new URLSearchParams({
    address: property.streetAddress,
    city: property.city,
    state: property.state,
    zipcode: property.zip,
  });
  return params.toString();
};
