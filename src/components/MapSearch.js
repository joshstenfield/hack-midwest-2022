// Autocomplete.js
import { Search } from "@mui/icons-material";
import { IconButton, InputBase, Paper } from "@mui/material";
import { Component } from "react";

class AutoComplete extends Component {
  constructor(props) {
    super(props);
    this.clearSearchBox = this.clearSearchBox.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedPlace !== this.props.selectedPlace) {
      this.searchInput.value = this.props.selectedPlace.formatted_address;
    }
  }

  componentDidMount({ map, mapApi } = this.props) {
    const options = {
      // restrict your search to a specific type of result
      types: ["address"],
      // restrict your search to a specific country, or an array of countries
      // componentRestrictions: { country: ['gb', 'us'] },
    };
    this.autoComplete = new mapApi.places.Autocomplete(
      this.searchInput,
      options
    );
    this.autoComplete.addListener("place_changed", this.onPlaceChanged);
    this.autoComplete.bindTo("bounds", map);
  }

  componentWillUnmount({ mapApi } = this.props) {
    mapApi.event.clearInstanceListeners(this.searchInput);
  }

  onPlaceChanged = ({ map, addplace } = this.props) => {
    const place = this.autoComplete.getPlace();

    if (!place.geometry) return;
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);
    }

    console.log(this.searchInput.value);
    this.searchInput.value = place.formatted_address;
    addplace(place);
    this.searchInput.blur();
  };

  clearSearchBox() {
    this.searchInput.value = "";
  }

  render() {
    return (
      <>
        <Paper
          component="form"
          sx={{
            display: "flex",
            alignItems: "center",
            width: "100%",
          }}
        >
          <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder="Search Google Maps"
            inputProps={{
              "aria-label": "search google maps",
              ref: (ref) => {
                this.searchInput = ref;
              },
            }}
          />
          <IconButton aria-label="search">
            <Search />
          </IconButton>
        </Paper>
      </>
    );
  }
}

export default AutoComplete;
