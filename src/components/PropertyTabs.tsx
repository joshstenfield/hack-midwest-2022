import { Box, Tab, Tabs, Typography } from "@mui/material";
import { useState } from "react";
import { NearbyProperties } from "./NearbyProperties";
import { PropertyCompare } from "./PropertyCompare";
import { PropertyInfo } from "./PropertyInfo";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {
        <Box style={{ visibility: value !== index ? "hidden" : "visible" }}>
          <Typography>{children}</Typography>
        </Box>
      }
    </div>
  );
}

interface PropertyTabsProps {
  property: any;
}
export const PropertyTabs: React.FC<PropertyTabsProps> = ({ property }) => {
  const [value, setValue] = useState(0);
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  function a11yProps(index: number) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  if (!property) {
    return null;
  }

  return (
    <>
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="basic tabs example"
        centered
      >
        <Tab value={0} label="Property Info" {...a11yProps(0)} />
        <Tab value={1} label="Nearby Addresses" {...a11yProps(1)} />
        <Tab value={2} label="Rental Info" {...a11yProps(2)} />
      </Tabs>
      <TabPanel index={0} value={value}>
        <PropertyInfo isOpen={value === 0} property={property} />
      </TabPanel>
      <TabPanel index={1} value={value}>
        <NearbyProperties isOpen={value === 1} property={property} />
      </TabPanel>
      <TabPanel index={2} value={value}>
        <PropertyCompare isOpen={value === 2} property={property} />
      </TabPanel>
    </>
  );
};
