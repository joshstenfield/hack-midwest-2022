import { CircularProgress, Grid, List, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { useEffect, useState } from "react";
import { propertyNearby } from "../apiOperations";
import { getPropertyHash } from "../helpers/hashHelper";

interface NearbyPropertiesProps {
  property: any;
  isOpen: boolean;
}

export const NearbyProperties: React.FC<NearbyPropertiesProps> = ({
  property,
  isOpen,
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [searchResults, setSearchResults] = useState<any>({});

  useEffect(() => {
    const search = async () => {
      const res = await propertyNearby(property);
      console.log(res);
      setSearchResults({
        ...searchResults,
        [getPropertyHash(property)]: res,
      });
      setIsLoading(false);
    };

    if (property && isOpen && !searchResults[getPropertyHash(property)]) {
      setIsLoading(true);
      search();
    }
  }, [isOpen, property, searchResults]);

  if (!property) {
    return null;
  }

  if (searchResults[getPropertyHash(property)] == null || isLoading) {
    return (
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <CircularProgress />
      </Box>
    );
  }

  if (searchResults[getPropertyHash(property)].addresses.length === 0) {
    return (
      <Box padding={4}>
        <Typography variant="h6">No nearby properties found.</Typography>
      </Box>
    );
  }

  return (
    <Box padding={4}>
      <Typography variant="h6">Details</Typography>

      <List dense disablePadding>
        <Grid container>
          {Object.keys(searchResults[getPropertyHash(property)].addresses)}
        </Grid>
      </List>
    </Box>
  );
};
