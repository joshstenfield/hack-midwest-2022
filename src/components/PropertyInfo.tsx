import {
  CircularProgress,
  Grid,
  List,
  ListItem,
  ListItemText,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { useEffect, useState } from "react";
import { searchProperty } from "../apiOperations";
import { getPropertyHash } from "../helpers/hashHelper";

interface PropertyInfoProps {
  property: any;
  isOpen: boolean;
}

export const PropertyInfo: React.FC<PropertyInfoProps> = ({
  property,
  isOpen,
}) => {
  const [searchResults, setSearchResults] = useState<any>({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const search = async () => {
      const res = await searchProperty(property);
      console.log(res);
      setSearchResults({
        ...searchResults,
        [getPropertyHash(property)]: res,
      });
      setIsLoading(false);
    };

    if (property && isOpen && !searchResults[getPropertyHash(property)]) {
      setIsLoading(true);
      search();
    }
  }, [isOpen, property, searchResults]);

  if (!property) {
    return null;
  }

  if (searchResults[getPropertyHash(property)] == null || isLoading) {
    return (
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <CircularProgress />
      </Box>
    );
  }

  return (
    <Box padding={4}>
      <Typography variant="h6">Details</Typography>

      <List dense disablePadding>
        <Grid container>
          {Object.keys(searchResults[getPropertyHash(property)].property)
            .reduce((acc, key) => {
              if (
                searchResults[getPropertyHash(property)].property[key] != null
              ) {
                acc.push(
                  <Grid item xs={12} sm={6} md={4} key={key}>
                    <ListItem key={key}>
                      <ListItemText
                        style={{ textTransform: "capitalize" }}
                        primary={key.replaceAll("_", " ")}
                        secondary={
                          searchResults[getPropertyHash(property)].property[key]
                        }
                      />
                    </ListItem>
                  </Grid>
                );
              }
              return acc;
            }, [] as any[])
            .map((item, index) => item)}
        </Grid>
      </List>
      <Typography variant="h6">History</Typography>
      <TableContainer component={Paper} style={{ width: "60%" }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Date</TableCell>
              <TableCell align="right">Description</TableCell>
              <TableCell align="right">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {searchResults[getPropertyHash(property)].history.map(
              (row: { date: string; description: string; type: string }) => (
                <TableRow
                  key={row.date + row.description + row.type}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.date}
                  </TableCell>
                  <TableCell align="right">{row.description}</TableCell>
                  <TableCell align="right">{row.type}</TableCell>
                </TableRow>
              )
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};
