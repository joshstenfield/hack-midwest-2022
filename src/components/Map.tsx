import GoogleMapReact from "google-map-react";
import { useEffect, useMemo, useRef, useState } from "react";
import env from "react-dotenv";
import AutoComplete from "./MapSearch";

type MarkerProps = {
  lat: number;
  lng: number;
  onClick: (lat: number, lng: number) => void;
};

function Marker({ lat, lng, onClick }: MarkerProps) {
  return (
    <img
      style={{
        position: "absolute",
        top: "-50px",
        left: "-25px",
      }}
      src="https://img.icons8.com/ios/50/000000/region-code.png"
      alt="marker"
      onClick={() => onClick(lat, lng)}
    />
  );
}

export type MapProps = {
  selectedPlace: any;
  setSelectedPlace: any;
};

export default function Map({ selectedPlace, setSelectedPlace }: MapProps) {
  const [mapApiLoaded, setMapApiLoaded] = useState(false);
  const [mapInstance, setMapInstance] = useState<any>();
  const [mapApi, setMapApi] = useState<any>();
  const [geocoder, setGeocoder] = useState<any>(null);

  const [center, setCenter] = useState<any>({
    lat: 39.092306123688125,
    lng: -94.58670048764,
  });

  const [zoom] = useState<number>(9);

  const poly = useRef<any>(null);

  const selectedLocation = useMemo(() => {
    return {
      lat: selectedPlace?.geometry?.location.lat() || null,
      lng: selectedPlace?.geometry?.location.lng() || null,
    };
  }, [selectedPlace]);

  // Get Current Location Coordinates
  const setCurrentLocation = () => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        setCenter({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
      });
    }
  };

  useEffect(() => {
    setCurrentLocation();
  }, []);

  const generateAddress = (lat: number, lng: number) => {
    const geocoder = new mapApi.Geocoder();

    geocoder.geocode(
      { location: { lat: lat, lng: lng } },
      (results: any, status: any) => {
        if (status === "OK") {
          if (results[0]) {
            setSelectedPlace(results[0]);
            setCenter({
              lat: lat,
              lng: lng,
            });
          } else {
            window.alert("No results found");
          }
        } else {
          window.alert("Geocoder failed due to: " + status);
        }
      }
    );
  };

  const addPlace = (place: any) => {
    const lat = place.geometry.location.lat();
    const lng = place.geometry.location.lng();
    generateAddress(lat, lng);
  };

  return (
    <>
      {mapApiLoaded && (
        <div
          style={{
            position: "absolute",
            top: selectedPlace ? "25%" : "50%",
            transform: "translateY(-50%)",
            left: "calc(50% - 40vh)",
            zIndex: 9999,
            width: "80vh",
          }}
        >
          <AutoComplete
            map={mapInstance}
            mapApi={mapApi}
            addplace={addPlace}
            selectedPlace={selectedPlace}
          />
        </div>
      )}
      <GoogleMapReact
        bootstrapURLKeys={{
          key: env.GOOGLE_MAPS_KEY,
          libraries: ["places", "geocoder"],
        }}
        onGoogleApiLoaded={({ map, maps }) => {
          setGeocoder(new maps.Geocoder());
          poly.current = new maps.Polygon({
            paths: [],
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
          });
          poly.current.setMap(map);
          setMapInstance(map);
          setMapApi(maps);
          setMapApiLoaded(true);
        }}
        yesIWantToUseGoogleMapApiInternals={true}
        defaultCenter={{ lat: -34.397, lng: 150.644 }}
        center={center}
        defaultZoom={8}
        zoom={zoom}
        onChange={({ zoom: newZoom, bounds, center: newCenter }) => {
          if (newCenter.lat !== center.lat) {
            setCenter(newCenter);
          } else if (newCenter.lng !== center.lng) {
            setCenter(newCenter);
          }
        }}
        onClick={({ lat, lng }) => {
          geocoder &&
            geocoder.geocode(
              { location: { lat, lng } },
              (results: any, status: any) => {
                setSelectedPlace(results[0]);
                setCenter({
                  lat: selectedPlace?.geometry?.location.lat(),
                  lon: selectedPlace?.geometry?.location.lng(),
                });
              }
            );
        }}
      >
        {selectedPlace && (
          <Marker
            lat={selectedLocation.lat}
            lng={selectedLocation.lng}
            onClick={(lat: number, lng: number) => {
              generateAddress(lat, lng);
            }}
          />
        )}
      </GoogleMapReact>
    </>
  );
}
