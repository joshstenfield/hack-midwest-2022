import {
  Box,
  CircularProgress,
  Grid,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { propertyCompareShortterm } from "../apiOperations";
import flattenDeep from "lodash/flattenDeep";
import { getPropertyHash } from "../helpers/hashHelper";

interface PropertyCompareProps {
  property: any;
  isOpen: boolean;
}

const getListChildren = (object: any, prevKey: string) => {
  return Object.keys(object)
    .reduce((acc, key) => {
      if (object[key] != null) {
        if (typeof object[key] === "object") {
          acc.push(getListChildren(object[key], key));
        } else {
          acc.push(
            <ListItemText
              style={{ textTransform: "capitalize" }}
              primary={key.replaceAll("_", " ")}
              secondary={object[key]}
            />
          );
        }
      }
      return acc;
    }, [] as any[])
    .map((item, index) => item);
};

export const PropertyCompare: React.FC<PropertyCompareProps> = ({
  property,
  isOpen,
}) => {
  const [searchResults, setSearchResults] = useState<any>({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const search = async () => {
      const res = await propertyCompareShortterm(property);
      console.log(res);
      setSearchResults({
        ...searchResults,
        [getPropertyHash(property)]: res,
      });
      setIsLoading(false);
    };

    if (property && isOpen && !searchResults[getPropertyHash(property)]) {
      setIsLoading(true);
      search();
    }
  }, [property, isOpen, searchResults]);

  const results = useMemo(() => {
    if (!property) {
      return null;
    }

    const result = searchResults[getPropertyHash(property)];
    if (!result || isLoading) {
      return null;
    }

    return Object.keys(result)
      .reduce((acc, key) => {
        if (result[key] != null) {
          const values = getListChildren(result[key], key);
          console.log(key);
          acc.push(
            <div key={key}>
              <Typography variant="h6" style={{ textTransform: "capitalize" }}>
                {key}
              </Typography>
              <Grid container>
                {flattenDeep(values).map((item: any) => {
                  return (
                    <Grid item xs={12} sm={6} md={4}>
                      <ListItem>{item}</ListItem>
                    </Grid>
                  );
                })}
              </Grid>
            </div>
          );
        }
        return acc;
      }, [] as any[])
      .map((item, index) => item);
  }, [isLoading, property, searchResults]);

  if (!property) {
    return null;
  }

  if (searchResults[getPropertyHash(property)] == null || isLoading) {
    return (
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <CircularProgress />
      </Box>
    );
  }

  return (
    <Box padding={4}>
      <List dense disablePadding>
        {results}
      </List>
    </Box>
  );
};
